#!/bin/bash
bin=(/bin /sbin /usr/bin /usr/sbin /usr/local/bin /usr/local/sbin)
lib=(/lib /usr/lib /usr/local/lib /usr/libexec)

for dir in "$bin" ; do
    [[ -d "$dir" ]] && chmod 755 -R "$dir"/*
done

for dir in "$lib" ; do
    [[ -d "$dir" ]] && chmod +x -R "$dir"/*
done
# syntax:
# u+s:/bin/filename
# 700:/lib/filename
cat /etc/fsinfo /etc/fsinfo.d/* 2>/dev/null | while read line ; do
    mode=$(echo $line | cut -f 1 -d ':')
    file=$(echo $line | cut -f 2 -d ':')
    [[ -f "$file" ]] && chmod --verbose "$mode" "$file"
done
