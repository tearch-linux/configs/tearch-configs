#!/usr/bin/python3
import os
### This functions are part of pardus-power-manager (with some changes)
# https://github.com/pardus/pardus-power-manager/blob/master/src/tools/profile.py
def readfile(file):
    f = open(file,"r")
    data = f.read()
    f.close()
    return data

def get_ac_online():
    if not os.path.exists("/sys/class/power_supply/"):
        return True
    if len(os.listdir("/sys/class/power_supply/")) == 0:
        return True
    for device in get_acpi_power_devices():
        if os.path.exists("/sys/class/power_supply/{}/status".format(device)):
            status = readfile("/sys/class/power_supply/{}/status".format(device)).lower().strip()
            if "discharging" in status:
                return False
            elif "not charging" in status:
                return True
            elif "charging" in status:
                return True
            elif "full" in status:
                return True
            elif "empty" in status:
                return False
            elif "unknown" in status:
                return True
    return True

def get_acpi_power_devices():
    devices = []
    for interface in os.listdir("/sys/bus/acpi/devices/"):
        acpi_device = "/sys/bus/acpi/devices/{}/power_supply".format(interface)
        if os.path.exists(acpi_device):
            for device in os.listdir(acpi_device):
                devices.append(device)
    return devices

# Exit status:
# 0 = AC connected
# 1 = AC not connected
if get_ac_online():
    exit(0)
exit(1)
