subdirs=fsmanager init-scripts config power
build:
	: run make install
install:
	for dir in $(subdirs) ; do \
	    make -C $$dir install ;\
	done
